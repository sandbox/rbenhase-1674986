<?php

/**
 * @file
 * Functions that are only called on the admin pages.
 */


/**
 * Overriding system settings form.
 */
function html5_audio_system_settings_form($form, $automatic_defaults = TRUE) {
  $form['actions']['#type'] = 'container';
  $form['actions']['#attributes']['class'][] = 'form-actions';
  $form['actions']['#weight'] = 100;
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  if ($automatic_defaults) {
    $form = _system_settings_form_automatic_defaults($form);
  }

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(
      t('The settings have not been saved because of the errors.'),
      'error');
  }
  $form['#submit'][] = 'system_settings_form_submit';
  // By default, render the form using theme_system_settings_form().
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'system_settings_form';
  }
  return $form;
}

/**
 * Module settings form.
 */
function html5_audio_admin_settings() {
  $form['html5_audio_library'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to the audio.js library'),
    '#description' => t('Edit only if you are sure of what you are doing.'),
    '#default_value' => variable_get('html5_audio_library',
      libraries_get_path('audiojs') . "/audio.min.js"),
  );
  return html5_audio_system_settings_form($form, FALSE);
}

/**
 * Implements hook_validate().
 */
function html5_audio_admin_settings_validate($form, &$form_state) {
  $error = array();
  // Trimming blank lines and such.
  $sf_library = preg_replace("/(^[\r\n]*|[\r\n]+)[\s  ]*[\r\n]+/", "\n",
  trim($form_state['values']['html5_audio_library']));
  $sf_library = explode("\n", $sf_library);
  foreach ($sf_library as $s) {
    if (!file_exists($s)) {
      $error[] = $s;
    }
  }

  if (!empty($error)) {
    $error_message = '';
    if (count($error) > 1) {
      foreach ($error as $e) {
        $error_message .= '<li>' . $e . '</li>';
      }
      $error_message = t('Files not found') . ': <ul>' . $error_message . '</ul>';
    }
    else {
      $error_message = t('File not found') . ': ' . $error[0];
    }
    form_set_error('html5_audio_library', $error_message);
  }
}
