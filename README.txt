
Welcome to Cross-Browser HTML5 Audio.

This module is a simple wrapper for the audio.js library, making it easier
to implement cross-browser MP3 audio on your Drupal site.

Audio.js provides a flash fallback which essentially allows you to use the
native HTML5 <audio> tags anywhere, without having to worry about browsers
that don't support HTML5 <audio> tags (i.e. older browsers) or lack support
for MP3 playback (i.e. Firefox, at least as of now); these browsers will
automatically degrade to the flash player, allowing you to avoid writing
extra code for cross-browser compatibility.

INSTALLATION

You must first download the audio.js library here: 
http://kolber.github.com/audiojs/

Extract the audiojs archive and copy the contents into your Drupal libraries
directory, so that the library itself (typically audio.js, or audio.min.js
for the minified version) can be found at the path specified on the module
configuration page (admin/config/media/html5_audio).

It's recommended that you place the entire contents of the extracted archive
in sites/all/libraries/audiojs, creating the "libraries" directory if it does
not already exist.

Note that the Audio.js library currently supports MP3 audio only.
